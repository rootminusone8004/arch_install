* Installation

Installation process works for *Arch Linux and there derivatives*.

** download packages

In order to run this program, you need the following packages to be installed.

- curl
#+begin_src bash
  sudo pacman -S curl
#+end_src
** download the scripts

Download the files:
#+begin_src bash
  curl -LO rootminusone8004.gitlab.io/arch_install/arch-before
#+end_src
Also, if you want to install desktop environment, download this as well:
#+begin_src bash
  curl -LO rootminusone8004.gitlab.io/arch_install/my-arch
#+end_src
